Redhat - NTTDATAaaa

Ansible playbook to download the binaries of Attunity Replicate, ODBC and to install them. To generate CSR certificate and its private key

Roles:
Below are the roles used in this playbook...

attunity_prerequisites -- Installs the basic pre-requisites required for executing this playbook.
attunity_install -- Downloads and insatlls the Attunity Replicate and sets password.
attunity_changedir(optional) -- Changes Data Directory 
attunity_odbc -- Downloads and insatlls ODBC and copies hortonworks.hiveodbc.ini file to /etc/.
attunity_Certificate(optional) -- Genarates CSR and private key

Note: The roles are to be executed in the order as per the playbook, as the components are dependant on the availability of each other. We have commented attunity_changedir and attunity_Certificate roles since it is optional.


Running Playbook:
# ansible-playbooks areplicate_main.yml 

Variables are defined in config.yml file ( Artifactory URLs,credentials,parameters to generate certificate )

Playbook Execution outline:
1. Checks the prerequisites (tar, gzip, unzip, python)

2. Downloads the binary of Attunity Replicate binary, installs it and sets the password

3. Changes Data Directory. This is kept as optional so that when the drive on which the current directory of Attunity Replicate's Data resides has insufficient space it will change the data directory to a desired directory.

4. Downloads the binary of ODBC, installs it and copies the hortonworks.hiveodbc.ini file to /etc/.

5. In order to automate csr certificate through Ansible we need pyOpenSSL-0.15.1-1.el7.noarch.rpm .Genarates CSR and private key, removes the Passphrase so as to send the CSR and private key to The Wintel team. This is also kept as optional.


